<?php

namespace App\Jobs;

use App\Entities\Photo;
use App\Services\PhotoService;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CropJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    public $photo;

    /**
     * CropJob constructor.
     * @param Photo $photo
     */
    public function __construct(Photo $photo)
    {
        $this->photo = $photo;

    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(PhotoService $photoService)
    {
            $originalPhoto = $this->photo->original_photo;

            $photoService->updateStatusProcessing($this->photo);

            $crop = $photoService->crop($originalPhoto, 100, 100);

            $newPhotoPath = $photoService->copyPhoto($originalPhoto, '100x100', $crop);

            $this->photo->photo_100_100 = $newPhotoPath;

            $crop = $photoService->crop($originalPhoto, 150, 150);

            $newPhotoPath = $photoService->copyPhoto($originalPhoto, '150x150', $crop);

            $this->photo->photo_150_150 = $newPhotoPath;

            $crop = $photoService->crop($originalPhoto, 250, 250);

            $newPhotoPath = $photoService->copyPhoto($originalPhoto, '250x250', $crop);

            $this->photo->photo_250_250 = $newPhotoPath;

            $photoService->updateStatusSuccess($this->photo);


    }

    /**
     * The job failed to process.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(\Exception $exception, PhotoService $photoService)
    {
        $photoService->updateStatusFailed($this->photo);
        $exception->getMessage();
    }

}
