<?php

namespace App\Values;

use Illuminate\Support\Facades\Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class PhotoData
{
    public $photo;
    public $status;
    public $userId;

    public function __construct(UploadedFile $photo, string $status, int $userId)
    {
        if (empty($photo)) {
            throw new \LogicException('Original photo is required');
        }

        if (empty($status)) {
            throw new \LogicException('Status is required');
        }

        if (empty($userId)) {
            throw new \LogicException('User id is required');
        }


        $this->userId = $userId;
        $this->photo = $this->savePhotoOnServer($photo);
        $this->status = $status;

    }

    private function savePhotoOnServer(UploadedFile $photo): string
    {
        $photoName = $photo->getClientOriginalName();
        $photo->move(Storage::path('images/'.$this->userId. '/'), $photoName);
        return 'images/'.$this->userId. '/'.$photoName;
    }
}
