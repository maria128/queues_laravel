<?php


namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;

class Photo extends Model
{

    const STATUS_UPLOADED = 'UPLOADED';
    const STATUS_PROCESSING = 'PROCESSING';
    const STATUS_SUCCESS = 'SUCCESS';
    const STATUS_FAIL = 'FAIL';

    protected $fillable = [
        'user_id', 'original_photo', 'photo_100_100',
        'photo_150_150', 'photo_250_250', 'status'
    ];

    /**
     * @var array
     */
    protected $photoStatus = [
        'UPLOADED',
        'PROCESSING',
        'SUCCESS',
        'FAIL'
    ];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @param int $value
     * @return string|null
     */
    public function getImageStatusAttribute($value)
    {
        return Arr::get($this->photoStatus, $value);
    }

    public function getUserId()
    {
        return $this->user_id;
    }

}
