<?php


namespace App\Repositories;


use App\Entities\Photo;
use  App\Repositories\Contracts\PhotoRepository as IPhotoRepository;

class PhotoRepository implements IPhotoRepository
{

    public function store(Photo $photo): Photo
    {
        $photo->save();

        return $photo;
    }
}

