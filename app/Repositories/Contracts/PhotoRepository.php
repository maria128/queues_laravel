<?php


namespace App\Repositories\Contracts;


use App\Entities\Photo;

interface PhotoRepository
{
    public function store(Photo $photo): Photo;
}