<?php

namespace App\Notifications;

use App\Entities\Photo;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ImageProcessedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public $photo;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Photo $photo)
    {

        $this->photo = $photo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast', 'mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $userName = $this->photo->user->name;
        $urlPhoto100X100 = url('/storage/files', [ $this->photo->photo_100_100]);
        $urlPhoto150X150 = url('/storage/files', [ $this->photo->photo_150_150]);
        $urlPhoto250X250 = url('/storage/files', [ $this->photo->photo_250_250]);

        return (new MailMessage)
            ->greeting('Dear ' . $userName . ',')
            ->line('Photos have been successfully uploaded and processed.')
            ->line('Here are links to the images:')
            ->line($urlPhoto100X100)
            ->line($urlPhoto150X150)
            ->line($urlPhoto250X250)
            ->line("Thanks!");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'status' => \strtolower($this->photo->status),
            'photo_100_100' => $this->photo->photo_100_100,
            'photo_150_150' => $this->photo->photo_150_150,
            'photo_250_250' => $this->photo->photo_250_250,
        ];
    }

    public function toBroadcast($notifiable)
    {
        return new BroadcastMessage(
            $this->toArray($notifiable)
        );
    }
}
