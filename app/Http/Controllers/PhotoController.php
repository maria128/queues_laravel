<?php

namespace App\Http\Controllers;

use App\Entities\Photo;
use App\Jobs\CropJob;
use App\Services\Contracts\AuthService;
use App\Services\Contracts\PhotoService;
use App\Values\PhotoData;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class PhotoController extends Controller
{

    private $authService;
    private $photoService;

    public function __construct(AuthService $authService, PhotoService $photoService)
    {
        $this->authService = $authService;
        $this->photoService = $photoService;
    }

    public function uploadImages(Request $request)
    {
        try {
            $data = $request->validate([
                "photo" => "required|mimes:jpeg,png,jpg,gif,svg|max:2048",
            ]);


            $photo = $this->photoService->savePhoto(new PhotoData(
                $data['photo'],
                Photo::STATUS_UPLOADED,
                $this->authService->getUser()->id
            ));

            CropJob::dispatch($photo);

            return response()->json([
                "status" => $photo->status,
                "original_photo" => $photo->original_photo,
            ], 200);
        } catch (ValidationException $e) {
            return response()->json(["error" => $e->errors()], 400);
        } catch (\LogicException $e) {
            return response()->json(["error" => $e->getMessage()], 400);
        } catch (\Exception $e) {
            return response()->json(["error" => 'something went wrong'], 400);
        }
    }

}
