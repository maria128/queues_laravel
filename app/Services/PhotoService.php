<?php

namespace App\Services;

use App\Entities\Photo;
use App\Notifications\ImageProcessedNotification;
use App\Notifications\ImageProcessingFailedNotification;
use App\Repositories\PhotoRepository;
use App\Services\Contracts\PhotoService as IPhotoService;
use App\Values\PhotoData;
use Illuminate\Contracts\Filesystem\Factory as Filesystem;
use App\Services\Contracts\AuthService;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class PhotoService implements IPhotoService
{
    private $authService;
    private $fileSystem;
    private $photoRepository;

    public function __construct(Filesystem $fileSystem, AuthService $authService, PhotoRepository $photoRepository)
    {
        $this->fileSystem = $fileSystem;
        $this->authService = $authService;
        $this->photoRepository = $photoRepository;
    }

    public function crop(string $pathToPhoto, int $width, int $height): string
    {
        $croppedImage = Image::make(
            Storage::path($pathToPhoto)
        )->crop($width, $height);
        $filePath = 'cropped/'
            . Str::random(40)
            . '.'
            . $croppedImage->extension;

        $this->fileSystem->put(
            $filePath,
            $croppedImage->stream()
        );

        return $filePath;
    }

    public function savePhoto(PhotoData $photoData): Photo
    {
        $photo = new Photo();
        $photo->original_photo = $photoData->photo;
        $photo->user_id = $photoData->userId;
        $photo->status = $photoData->status;
        $photo->save();

        $this->photoRepository->store($photo);

        return $photo;
    }


    public function copyPhoto($oldPath, $newNameFile, $cropPhoto):string
    {
        $infoPhoto = pathinfo($oldPath);
        $extension = $infoPhoto['extension'];
        $filename = $infoPhoto['filename'];
        $userId = $this->authService->getUser()->id;

        $newPhotoPath = 'images/' . $userId . '/'. $filename . $newNameFile .'.' . $extension;

        $this->fileSystem->move($cropPhoto, $newPhotoPath);

        return $newPhotoPath;
    }

    public function updateStatusProcessing(Photo $photo)
    {
        $photo->status = Photo::STATUS_PROCESSING;
        $this->photoRepository->store($photo);


        Notification::send($photo->user, new ImageProcessedNotification($photo));
    }

    public function updateStatusSuccess(Photo $photo)
    {
        $photo->status = Photo::STATUS_SUCCESS;
        $this->photoRepository->store($photo);

        Notification::send($photo->user, new ImageProcessedNotification($photo));
    }

    public function updateStatusFailed(Photo $photo)
    {
        $photo->status = Photo::STATUS_FAIL;
        $photo->photo_250_250 = '';
        $photo->photo_150_150 = '';
        $photo->photo_100_100 = '';
        $this->photoRepository->store($photo);

        Notification::send($photo->user, new ImageProcessingFailedNotification($photo));
    }


}
