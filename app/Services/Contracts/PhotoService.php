<?php

namespace App\Services\Contracts;

use App\Entities\Photo;
use App\Values\PhotoData;

interface PhotoService
{
    public function crop(string $pathToPhoto, int $width, int $height): string;
    public function savePhoto(PhotoData $photoData): Photo;
}
